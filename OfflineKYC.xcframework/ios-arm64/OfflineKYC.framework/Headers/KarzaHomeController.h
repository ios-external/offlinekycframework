//
//  KarzaHomeController.h
//  liveness
//
//  Created by Keval Shah on 5/23/20.
//  Copyright © 2020 Keval Shah. All rights reserved.
//

#import <UIKit/UIKit.h>

@class KarzaHomeController;             //define class, so protocol can see class
@protocol KarzaLivenessDelegate <NSObject>   //define delegate protocol
    - (void) getLivenessScore: (float)score
                                livenessImage:(UIImage *)image
                                resultInfo:(NSDictionary *)result;  //define delegate method to be implemented within another class
@end //end protocol

@interface KarzaHomeController : UIViewController

+ (instancetype)presentKarzaHomeController:(UIViewController *)controller
                                            karzaToken:(NSString *)token;

@property (nonatomic, weak) id <KarzaLivenessDelegate> delegate;
@property (nonatomic, weak) NSString *karzaToken;
@property (nonatomic, strong) NSString *licensePath;
@property (nonatomic, strong) NSString *karzaEnvironment;

@end

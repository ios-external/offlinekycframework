//
//  OfflineKYC.h
//  OfflineKYC
//
//  Created by Keval Shah on 23/03/21.
//

#import <Foundation/Foundation.h>


//! Project version number for OfflineKYC.
FOUNDATION_EXPORT double OfflineKYCVersionNumber;

//! Project version string for OfflineKYC.
FOUNDATION_EXPORT const unsigned char OfflineKYCVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <OfflineKYC/PublicHeader.h>

#import <OfflineKYC/KarzaHomeController.h>
